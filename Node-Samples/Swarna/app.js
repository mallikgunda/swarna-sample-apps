const { create } = require('domain');
const http = require('http');

const hostname = "localhost";
const port = 3000;

const createSerer = http.createServer((req,res) =>{
    let name = req.read.name;
    res.setHeader("Content-Type","application/json");
    res.statusCode = 200;
    res.end(`Hello World One ${name}`);
});

createSerer.listen(port,hostname, () =>{
    console.log(`Server running on port http://${hostname}:${port}`);
});
