const express = require("express");
const os = require("os");


const app = express();
const port = 4000;
app.get('/',(req,res) =>{
    res.send("OS Examples");
    console.log(`os arch: ${os.version}`);
    console.log(`os hostname ${os.hostname}`);
    console.log(`os totMemory ${os.totalmem}`);
    console.log(`os freemem ${os.freemem}`);
})


app.listen(port, ()=>{
    console.log(`server running on port: http://localhost:${port}`);
})