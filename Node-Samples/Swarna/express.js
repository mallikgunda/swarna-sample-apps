const express = require("express");
const fs = require("fs");
const { isBuffer } = require("util");
//import fs from "fs";
const app = express();
const port = 3000;

app.get('/',(req,res) =>{
    res.send("Express JS Server");
});

app.get('/student',(req,res)=>{
    res.send("Student Service");
});

app.post('/teacher', (req,res)=>{
    res.send("Teacher Service");
});

app.get('/student.text', (req,res) =>{
    fs.readFile("./fs.text", (err,data) =>{
        if(err){
            console.log("Err ion reading file content");
        }
        const content = data;
        res.send(content);
    });
    
})

app.get('/writeToFile',(req,res) =>  {
   /* fs.writeFile("./abc.txt", "TextToFile", (err)=>{
        if(err){
            console.log("Error in writing contents to file");
        }
       
    }) */
   
    const appendContent = "Some Content";

    fs.appendFile("/abc.txt",appendContent, (err) =>{
        if(err){
            console.log("error in appending content to file " +err);
        }
    })
    res.send("FileWrite success") ;


});

app.get

app.listen(port,()=>{
    console.log(`Server running on http://localhost:${port}`);
    });